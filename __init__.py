#!/usr/bin/env python3
# coding=utf-8
"""
The purpose of Variabilitectron is revealing unnoticed variability into XMM-Newton observations.
Variabilitectron comes into two sub-programmes:
a first one calculating the variability and detecting the variable areas for a given observation 
cleaned from high background;
a second one rendering an image of the variability computed.
"""
__author__ = "Damien Wojtowicz (2017) - damien.wojtowicz@gmail.com"
__contact__ = "Natalie Webb (IRAP) - natalie.webb@irap.omp.eu"
__version__ = "1.0"
__date__ = "July 2017"
__license__ = "GNU Affero General Public License v3.0"
__package__ = "Variabilitectron"
