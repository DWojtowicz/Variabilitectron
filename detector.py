#!/usr/bin/env python3
# coding=utf-8

###################################################################################################
#                                                                                                 #
# Variabilitectron - A programme to search variability into X ray sources observed by XMM-Newton  #
#                                                                                                 #
# DETECTOR main programme                                                                         #
#                                                                                                 #
# Damien Wojtowicz (2017) - damien.wojtowicz@gmail.com                                            #
#                                                                                                 #
###################################################################################################
"""
Detector's main programme
"""

# Built-in imports

import sys
import os
import time
from functools import partial

# Third-party imports

from multiprocessing import Pool
from astropy.io import fits
import numpy as np

# Internal imports

from fits_extractor import *
from variability_utils import *
import file_names as FileNames
from file_utils import *


###################################################################################################
#                                                                                                 #
#                                                                                                 #
# Global constants                                                                                #
#                                                                                                 #
#                                                                                                 #
###################################################################################################


BOX_SIZE = 3
"""
Side of the squared box used for the detection of variable areas.\n
Type: int\n
Unit: pixels
"""

DETECTION_LEVEL = 1.0
"""
det_lvl into the following formula (see documentation): det_lvl × |b|² × low_lvl\n
"""

TIME_WINDOW = 900.0
"""
Duration of time windows used in variability calculation.\n
Type: float\n
Unit: seconds
"""

SIGNIFICANCE_LEVEL_TW = 0.75
"""
Minimal ratio of available data during a TW needed to consider a TW acceptable and
to calculate a projection of its value.\n
"""

MAX_THREADS_ALLOWED = 12
"""
Maximal number of thread the programme is allowed to use.\n
"""

ADDITIONAL_TW = 2
"""
Factor of additional TWs to create by running count_events starting at a shifted t0 time.\n
"""

METHOD = 'average'
"""
Method used for variability computation. Can be set to 'median' or to 'average'
"""


###################################################################################################
#                                                                                                 #
#                                                                                                 #
# Utilities                                                                                       #
#                                                                                                 #
#                                                                                                 #
###################################################################################################


def print_help() :
    """
    Procedure printing the terminal help
    """
    print("USE:")
    print("\tpython3.5 main.py <FITS EVENTS FILE> <FITS GTI FILE> <OUTPUT DIRECTORY> <Options>")
    print("")
    print("Options:")
    print("--box-size <Integer>\n\tSize of the detection box. Default: 3 px²")
    print("--detection-level <Float>\n\tThe number of times the median variability is required to trigger a detection. Default: 0.75")
    print("--time-window <Float>\n\tThe duration of the time windows. Default: 500.0 s")
    print("--significance-level-tw <Float>\n\tRatio of acceptability for a time window. Shall be between 0.0 and 1.0 Default: 0.7")
    print("--max-threads-allowed <Integer>\n\tMaximal number of thread the program is allowed to use. Default: 12")
    print("--additional-tw <Integer>\n\tNombre de passes à effectuer avec un décalage proportionnel à celui de la TW. Default: 2")
    print('--method\n\tMethod used for computations. Can be either \'average\'(default) or \'median\'.')


###################################################################################################


def parse_arguments() :
    """
    Procedure parsing the optional arguments
    """

    # There is at least 3 aruments
    if len(sys.argv) > 4 and len(sys.argv) % 2 == 0:
        i = 4

        # Going throught the arguments list
        while i < len(sys.argv) :

            # Case of the box size : the size of the box shall be positive and smaller than a CCD
            if sys.argv[i] == "--box-size":

                try :
                    BOX_SIZE = int(sys.argv[i+1])

                    # The maximal value for RAWX is 64
                    if BOX_SIZE <= 1 and BOX_SIZE > 64:
                        raise

                except :
                    print("Box size")
                    return False

            # The detection level shall be a positive floating number
            elif sys.argv[i] == "--detection-level" :
                
                try :
                    DETECTION_LEVEL = float(sys.argv[i+1])

                    if DETECTION_LEVEL < 0.0 :
                        raise

                except :
                    print("detlvl")
                    return False

            # A time window shall be of a positive duration.
            elif sys.argv[i] == "--time-window" :
                
                try :
                    TIME_WINDOW = float(sys.argv[i+1])

                    if TIME_WINDOW < 0.0 :
                        raise
                        
                except :
                    print("tw")
                    return False


            # Shall be between 0.0 and 1.0
            elif sys.argv[i] == "--significance-level-tw" :
                
                try :
                    SIGNIFICANCE_LEVEL_TW = float(sys.argv[i+1])

                    if SIGNIFICANCE_LEVEL_TW < 0.0 or SIGNIFICANCE_LEVEL_TW > 1.0:
                        raise

                except :
                    print("sigtw")
                    return False


            # There shall always be at least one thread.
            elif sys.argv[i] == "--max-threads-allowed" :
                
                try :
                    MAX_THREADS_ALLOWED = int(sys.argv[i+1])

                    if MAX_THREADS_ALLOWED < 1 :
                        raise
                                  
                except :
                    print("threads")
                    return False


            # There shall always be at least one thread.
            elif sys.argv[i] == "--additional-tw" :
                
                try :
                    ADDITIONAL_TW = int(sys.argv[i+1])

                    if ADDITIONAL_TW < 0 :
                        raise
                                  
                except :
                    print("add tw")
                    return False

            elif sys.argv[i] == "--method" :
                
                try :
                    METHOD = sys.argv[i+1]

                    if METHOD not in ['averge', 'median'] :
                        raise
                                  
                except :
                    print("add tw")
                    return False

            # There is an error in the arguments passed.
            else :
                return False
                    
            i += 2

    elif len(sys.argv) != 4 :
        return False

    return True




###################################################################################################
#                                                                                                 #
#                                                                                                 #
# Main programme                                                                                  #
#                                                                                                 #
#                                                                                                 #
###################################################################################################

def main_median() :
    """
    Main function of the detector using median technique
    """
####
#
#  Preliminaries
#
####

    # Checking arguments
    if not parse_arguments() :
        print_help()
        exit(-1)

    # Counter for the overall execution time
    original_time = time.time()

    # Opening the output files
    log_f, var_f, var_per_tw_f, detected_var_areas_f, tws_f, detected_var_sources_f = open_files(sys.argv[3])

    log_f.write('Command:\n\t')
    log_f.write(' '.join([arg for arg in sys.argv]))
    log_f.write('\n')

    log_f.write("Creation of output files over.\n")

    # Recovering the EVENTS list
    try :
        data = extraction_photons(sys.argv[1])
    
    except Exception as e:
        log_f.write("!!!!\nImpossible to extract photons. ABORTING.")
        close_files()
        exit(-2)

    # Recovering GTI list
    try:
        gti_list = extraction_deleted_periods(sys.argv[2])

    except Exception as e:
        log_f.write("!!!!\nImpossible to extract gti. ABORTING.")
        close_files()
        exit(-2)

    log_f.write("Extraction from FITS over.\n")


####
#
#   Counting events
#
####

    M_mat = []
    time_windows = []

    t0_observation = min([evt['TIME'] for ccd in data for evt in ccd])

    for ccd in range(12) :
        log_f.write('\tTimed_events CCD {0}\n'.format(ccd))
        M_mat_tmp, tws_tmp = count_events(data[ccd], gti_list, TIME_WINDOW, ADDITIONAL_TW, SIGNIFICANCE_LEVEL_TW, t0_observation)
        M_mat.append(M_mat_tmp)
        time_windows.append(tws_tmp)
     
    # Removing additional time window on certain CCDs
    real_number_tw = min([len(tws) for tws in time_windows])

    log_f.write('Number of TWs\n\t{0}'.format(real_number_tw))

    for ccd in range(12) :
        if len(time_windows[ccd]) > real_number_tw :
            log_f.write('\tCCD {0} had too many time windows\n'.format(ccd + 1))

            del time_windows[ccd][-1]

            for a in range(len(M_mat[ccd])) :
                for b in range(len(M_mat[ccd][a])) :
                    del M_mat[ccd][a][b][-1]

    # Case when there is no TW : the programme will break if it continues.
    if len(time_windows[0]) <= 1 :
        log_f.write("!!!!\tThere is no acceptable time window. ABORTING.\n")
        close_files()
        exit(1)

    # Writing time windows to their file
    cpt = 1
    for tw in time_windows[0] :
        tws_f.write('{0};{1}\n'.format(cpt, tw))
        cpt += 1

    # Writing counter for each pixel for each TW
    for ccd in range(12) :
        for rawx in M_mat[ccd] :
            for rawy in rawx:
                var_per_tw_f.write(';'.join([str(tw) for tw in rawy]))
                var_per_tw_f.write('\n')


####
#
#   Computing variability
#
####
    
    # Computing v_matrix
    v_matrix = []
    with Pool(MAX_THREADS_ALLOWED) as p:
        v_matrix = p.map(variability_calculation, M_mat)

    # Writing variability for each pixel
    for ccd in range(12) :
        for i in range(64):
            for j in range(200):
                var_f.write(str(v_matrix[ccd][i][j]) + '\n')


####
#
#   Detecting variable areas and sources
#
####


    median = np.median([v_matrix[ccd][i][j] for ccd in range(12) for i in range(len(v_matrix[ccd])) for j in range(len(v_matrix[ccd][i]))])
    
    # Avoiding a too small median value for detection
    if median < 0.75 :
        median = 0.75
        log_f.write('Median switched to 0.75. \n')

    variable_areas = []

    # Currying the function for the pool of threads
    variable_areas_detection_partial = partial(variable_areas_detection, median, BOX_SIZE, DETECTION_LEVEL)

    # Performing parallel detection on each CCD
    with Pool(MAX_THREADS_ALLOWED) as p:
        variable_areas = p.map(variable_areas_detection_partial, v_matrix)

    # Writing sources to their files
    cpt_source = 0
    for ccd in range(12) :
        for source in variable_areas[ccd] :
            cpt_source += 1

            center_x = sum([p[0] for p in source]) / len(source)
            center_y = sum([p[1] for p in source]) / len(source)
            
            detected_var_sources_f.write('{0};{1};{2};{3}\n'.format(cpt_source, ccd + 1, center_x, center_y))

            for p in source :
                detected_var_areas_f.write('{0};{1};{2};{3}\n'.format(cpt_source, ccd + 1, p[0], p[1]))


####
#
#   End of the programme
#
####
    log_f.write("# TEMPS TOTAL D'EXÉCUTION : %s secondes\n" % (time.time() - original_time))
    close_files(log_f, var_f, var_per_tw_f, detected_var_areas_f, tws_f, detected_var_sources_f)
    


###################################################################################################


def main_average() :
    """
    Implementation of detection using average technique.\n
    This function was developed in the rush of the last day of my placement,
    using the "La RACHE" development method (see http://www.la-rache.com/).
    I apologize for the code repetition with the function above and for the potential dirtiness
    of that code.
    """
    # Counter for the overall execution time
    original_time = time.time()

    # Opening the output files
    log_f, var_f, var_per_tw_f, detected_var_areas_f, tws_f, detected_var_sources_f = open_files(sys.argv[3])

    log_f.write('Command:\n\t')
    log_f.write(' '.join([arg for arg in sys.argv]))
    log_f.write('\n')

    log_f.write("Creation of output files over.\n")

    # Recovering the EVENTS list
    try :
        data = extraction_photons(sys.argv[1])
    
    except Exception as e:
        log_f.write("!!!!\nImpossible to extract photons. ABORTING.")
        close_files()
        exit(-2)

    # Recovering GTI list
    try:
        gti_list = extraction_deleted_periods(sys.argv[2])

    except Exception as e:
        log_f.write("!!!!\nImpossible to extract gti. ABORTING.")
        close_files()
        exit(-2)

    log_f.write("Extraction from FITS over.\n")

    v_matrix = []
    time_windows = []

    t0_observation = min([evt['TIME'] for ccd in data for evt in ccd])
    var_calc_partial = partial(variability_computation_avg, gti_list, TIME_WINDOW, SIGNIFICANCE_LEVEL_TW, t0_observation)
    

    with Pool(MAX_THREADS_ALLOWED) as p:
        v_matrix = p.map(var_calc_partial, data)

    for ccd in v_matrix :
        for rawx in v_matrix :
            for rawy in rawx:
                for px in rawy :
                    var_f.write('{0}\n'.format(px))
                    print(px)

    median = np.median([px for ccd in v_matrix for rawx in ccd for px in rawx])
    
    # Avoiding a too small median value for detection
    if median < 0.75 :
        median = 0.75
        log_f.write('Median switched to 0.75. \n')

    variable_areas = []

    # Currying the function for the pool of threads
    variable_areas_detection_partial = partial(variable_areas_detection, median, BOX_SIZE, DETECTION_LEVEL)

    # Performing parallel detection on each CCD
    with Pool(MAX_THREADS_ALLOWED) as p:
        variable_areas = p.map(variable_areas_detection_partial, v_matrix)


    cpt_source = 0
    for ccd in range(12) :
        for source in variable_areas[ccd] :
            cpt_source += 1

            center_x = sum([p[0] for p in source]) / len(source)
            center_y = sum([p[1] for p in source]) / len(source)
            
            detected_var_sources_f.write('{0};{1};{2};{3}\n'.format(cpt_source, ccd + 1, center_x, center_y))

            for p in source :
                detected_var_areas_f.write('{0};{1};{2};{3}\n'.format(cpt_source, ccd + 1, p[0], p[1]))

    log_f.write("# TEMPS TOTAL D'EXÉCUTION : %s secondes\n" % (time.time() - original_time))            
    close_files(log_f, var_f, var_per_tw_f, detected_var_areas_f, tws_f, detected_var_sources_f)
    


###################################################################################################


if __name__ == '__main__':
    # Checking arguments
    if not parse_arguments() :
        print_help()
        exit(-1)

    if METHOD == 'average':
        main_average()

    elif METHOD == 'median':
        main_median()