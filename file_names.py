#!/usr/bin/env python3
# coding=utf-8


###################################################################################################
#                                                                                                 #
# Variabilitectron - A programme to search variability into X ray sources observed by XMM-Newton  #
#                                                                                                 #
# Declaration of file names                                                                       #
#                                                                                                 #
# Damien Wojtowicz (2017) - damien.wojtowicz@gmail.com                                            #
#                                                                                                 #
###################################################################################################
"""
Declaration of the file names handled both by the detector and the renderer
"""

LOG = "log.txt"

VARIABILITY = "variability_file.csv"
EVTS_PX_TW = "events_count_per_px_per_tw.csv"
VARIABLE_AREAS = "detected_variable_areas_file.csv"
TIME_WINDOWS = "time_windows_file.csv"
VARIABLE_SOURCES = "detected_variable_sources.csv"

OUTPUT_IMAGE = "variability.pdf"
OUTPUT_ANIMATION = "animation.gif"
OUTPUT_IMAGE_SRCS = "sources.pdf"