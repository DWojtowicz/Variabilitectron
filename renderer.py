#!/usr/bin/env python3
# coding=utf-8

###################################################################################################
#                                                                                                 #
# Variabilitectron - A programme to search variability into X ray sources observed by XMM-Newton  #
#                                                                                                 #
# RENDERER main programme                                                                         #
#                                                                                                 #
# Damien Wojtowicz (2017) - damien.wojtowicz@gmail.com                                            #
#                                                                                                 #
###################################################################################################
"""
Renderer's main programme
"""

# Built-in imports

from os.path import sys
import os
import shutil

# Third-party imports

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors
from pylab import figure, cm
from matplotlib.colors import LogNorm
import imageio

# Internal imports

import file_names as FileNames
from file_utils import *


###################################################################################################
#                                                                                                 #
#                                                                                                 #
# Utilities                                                                                       #
#                                                                                                 #
#                                                                                                 #
###################################################################################################


def preprocess_data(input_data) :
    """
    Function building a matrix for the output image from a list of data built that way ;
    [dot for ccd in data for rawx in ccd for dot in rawx] 
    @param input_data: A list
    @return: A 384*400 matrix
    """

    data = []

    # Generating the basis of the array
    for i in range(64*6) :
        data.append([])

    # Building the ccd data list
    # CCD order to follow :
    # 6     reversed(9)
    # 5     reversed(8)
    # 4     reversed(7)
    # 1     reversed(10)
    # 2     reversed(11)
    # 3     reversed(12)

    # Appending CCD 6 data

    for i in range(320, 384) :
        data[i % 64].extend(input_data[i])


    # Appending CCD 5 data
    for i in range(256, 320) :
        data[(i % 64) + 64].extend(input_data[i])

    # Appending CCD 4 data
    for i in range(192, 256) :
        data[(i % 64) + 128].extend(input_data[i])


    # Appending CCD 1 data
    for i in range(0, 64) :
        data[(i % 64) + 192].extend(input_data[i])


    # Appending CCD 2 data
    for i in range(64, 128) :
        data[(i % 64) + 256].extend(input_data[i])


    # Appending CCD 3 data
    for i in range(128, 192) :
        data[(i % 64) + 320].extend(input_data[i])


    # Appending CCD 9 data
    i = 575
    while i >= 512 :
        data[i % 64].extend(reversed(input_data[int(512 + 31.5 + (512 + 31.5 - i))]))
        i -= 1


    # Appending CCD 8 data
    i = 511
    while i >= 448 :
        data[(i % 64) + 64].extend(reversed(input_data[int(448 + 31.5 + (448 + 31.5 - i))]))
        i -= 1

    # Appending CCD 7 data
    i = 447
    while i >= 384 :
        data[(i % 64) + 128].extend(reversed(input_data[int(384 + 31.5 + (384 + 31.5 - i))]))
        i -= 1

    # Appending CCD 10 data
    i = 639
    while i >= 576 :
        data[(i % 64) + 192].extend(reversed(input_data[int(576 + 31.5 + (576 + 31.5 - i))]))
        i -= 1

    # Appending CCD 11 data
    i = 703
    while i >= 640 :
        data[(i % 64) + 256].extend(reversed(input_data[int(640 + 31.5 + (640 + 31.5 - i))]))
        i -= 1

    # Appending CCD 12 data
    i = 767
    while i >= 704 :
        data[(i % 64) + 320].extend(reversed(input_data[int(704 + 31.5 + (704 + 31.5 - i))]))
        i -= 1

    print(len(data))
    for ccd in range(12):
        print('\t{0}'.format(len(data[ccd])))
        print('\t\t{0}'.format(sum(data[ccd])))

    return data


###################################################################################################


def render_variability_whole_image(data, sources, output_file, maximum_value=None) :
    """
    Function rendering an from the matrix data.
    @param data: The matrix to render
    @param sources: The detected sources
    @param output_file: The path to the PDF file to be created
    @param maximum_value: The maximal value for the logarithmic scale
    """

    # Limite maximale de l'échelle des couleurs pour la normalisation par logarithme
    if maximum_value == None :
        maximum_value = max([max(tmp) for tmp in data])

    # min_log = np.median([max(tmp) for tmp in data]) / 3.0


    f = figure()
    ax = f.add_axes([0.17, 0.02, 0.72, 0.79])
    im = ax.matshow(data, cmap=cm.plasma_r, norm=LogNorm(vmin=2.0, vmax=maximum_value), extent=[0, 400, 0, 384])
    f.colorbar(im, ticks=np.arange(0, 500, 2))
    
    # Ajout des grilles
    maj_x_tick = np.arange(0, 400, 200) # La grille majeure correspond aux CCD
    maj_y_tick = np.arange(0, 400, 64)  # La grille mienure est composée de carrés de taille 16x16
    min_x_tick = np.arange(0, 400, 16)
    min_y_tick = np.arange(0, 400, 16)
    ax.set_xticks(maj_x_tick)
    ax.set_xticks(min_x_tick, minor=True)
    ax.set_yticks(maj_y_tick)
    ax.set_yticks(min_y_tick, minor=True)
    ax.grid(which='both', color='k')
    ax.grid(which='minor', alpha=0.65, linestyle=':')
    ax.grid(which='major', linestyle='-')

    # Ajout du numéro correspondant au CCD
    ax.text(2, 383, "6", horizontalalignment='left', verticalalignment='top', alpha=0.3)
    ax.text(2, 319, "5", horizontalalignment='left', verticalalignment='top', alpha=0.3)
    ax.text(2, 255, "4", horizontalalignment='left', verticalalignment='top', alpha=0.3)
    ax.text(2, 0, "3", horizontalalignment='left', verticalalignment='bottom', alpha=0.3)
    ax.text(2, 64, "2", horizontalalignment='left', verticalalignment='bottom', alpha=0.3)
    ax.text(2, 128, "1", horizontalalignment='left', verticalalignment='bottom', alpha=0.3)

    ax.text(398, 383, "9", horizontalalignment='right', verticalalignment='top', alpha=0.3)
    ax.text(398, 319, "8", horizontalalignment='right', verticalalignment='top', alpha=0.3)
    ax.text(398, 255, "7", horizontalalignment='right', verticalalignment='top', alpha=0.3)
    ax.text(398, 0, "12", horizontalalignment='right', verticalalignment='bottom', alpha=0.3)
    ax.text(398, 64, "11", horizontalalignment='right', verticalalignment='bottom', alpha=0.3)
    ax.text(398, 128, "10", horizontalalignment='right', verticalalignment='bottom', alpha=0.3)

    if sources != None :
        src_x = [int(src.x) for src in sources]
        src_y = [int(src.y) for src in sources]
        
        plt.plot(src_x, src_y, 'o', alpha=0.3)

    f.savefig(output_file, pad_inches=0, bbox_inches='tight', dpi=500)


###################################################################################################


def render_variability_gif(counts, output_file, number_TWs, temporary_files_folder="./__tmp_gif__", frame_duration=0.1):
    """
    Function rendering an animated image of the counters for each time window.
    It renders each image into a temporary folder, and deletes them once the GIF is generated.
    @param counts: The set of matrix M round
    @param output_file: The path to the GIF file to be created
    @param number_TWs: The number of time windows
    @param temporary_files_folder: The path to the temporary folder
    @param frame_duration: The duration of each frame.
    """

    if not os.path.exists(temporary_files_folder):
        os.makedirs(temporary_files_folder)

    cpt_frames = 0
    index_frame = 0


    max_value = max([tw for line in counts for px in line for tw in px ])

    images = []
    for i in range(number_TWs) :
        render_variability_whole_image(
            [[px[i] for px in line] for line in counts], \
            None, \
            temporary_files_folder + "/" + str(i) + ".jpg", \
            maximum_value=max_value)

        images.append(imageio.imread(temporary_files_folder + "/" + str(i) + ".jpg"))

    imageio.mimsave(output_file, images, duration=frame_duration)

    # Nettoyage
    shutil.rmtree(temporary_files_folder)


###################################################################################################


def print_help_r() :
    """
    Prints the help of the renderer
    """
    print("Use:")
    print("\tpython3 renderer.py <PATH_TO_DETECTOR_OUTPUT> [--gif]")
    print("Arguments:")
    print("\t--gif\tGenerates an animation of the counted events per pixel.\n\t\t\tWARN: Can be very memory consuming.")



###################################################################################################
#                                                                                                 #
#                                                                                                 #
# Main programme                                                                                  #
#                                                                                                 #
#                                                                                                 #
###################################################################################################


def main_r() :
    """
    Main function of the renderer
    """
    if len(sys.argv) > 3 :
        print_help_r()
        exit(-1)

    DO_GIF = False

    if len(sys.argv) == 3 :
        if sys.argv[2] != "--gif" :
            print_help()
            exit(-1)
        else :
            DO_GIF = True



    INPUT_FOLDER = sys.argv[1]

    if INPUT_FOLDER[-1] != "/" :
        INPUT_FOLDER += "/"

    var_per_px = read_from_file(INPUT_FOLDER + FileNames.VARIABILITY)
    print(len(var_per_px))

    if DO_GIF :
        var_per_px_per_tw = read_from_file(INPUT_FOLDER + FileNames.EVTS_PX_TW, raw=True)
        tws = read_tws_from_file(INPUT_FOLDER + FileNames.TIME_WINDOWS)
    
    sources = read_sources_from_file(INPUT_FOLDER + FileNames.VARIABLE_SOURCES)

    data_matrix = preprocess_data(var_per_px)
    render_variability_whole_image(data_matrix, None, INPUT_FOLDER + FileNames.OUTPUT_IMAGE)
    render_variability_whole_image(data_matrix, sources, INPUT_FOLDER + FileNames.OUTPUT_IMAGE_SRCS)

    if DO_GIF :
        data_matrix = preprocess_data(var_per_px_per_tw)
        render_variability_gif(data_matrix, INPUT_FOLDER + FileNames.OUTPUT_ANIMATION, len(tws), temporary_files_folder=INPUT_FOLDER+"__tmp_gif__/")


###################################################################################################


if __name__ == '__main__':
    main_r()