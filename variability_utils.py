#!/usr/bin/env python3
# coding=utf-8


###################################################################################################
#                                                                                                 #
# Variabilitectron - A programme to search variability into X ray sources observed by XMM-Newton  #
#                                                                                                 #
# DETECTOR utilities                                                                              #
#                                                                                                 #
# Damien Wojtowicz (2017) - damien.wojtowicz@gmail.com                                            #
#                                                                                                 #
###################################################################################################
"""
Implementation of variability-related procedures specified into the documentation
"""

# Third-party imports

import numpy
from numpy import inf

###################################################################################################
#                                                                                                 #
#                                                                                                 #
# Procedure count_events                                                                          #
#                                                                                                 #
#                                                                                                 #
###################################################################################################


def __perform_count_events(data, gti, time_interval, time_offset, acceptable_ratio, start_time) :
    """
    Function implementing the procedure count_events.
    @param  data:    E round, the list of events sorted by their TIME attribute
    @param  gti:     G round, the list of TW cut-off the observation
    @param  time_interval:   The duration of a time window
    @param  time_offset:   The offset to add to t0
    @param  acceptable_ratio:  The acceptability ratio for a TW
    @param  start_time:  The t0 instant of the observation
    @return: The matrix M round; The list of the kept TWs
    """
    index_beg = 0
    index_end = 0

    # Building the output tensor
    counted_events = []
    time_windows = []

    for i in range(64) :
        counted_events.append([])
        
        for j in range(200) :
            counted_events[i].append([])


    i = 0 # Counter for data points
    j = 0 # Counter for cutted-off periods
    k = 0 # Counter of time frames

    projection_ratio = 1.0
    time_skipped = 0.0

    time_limit = start_time + time_offset

    # Skipping the first points of data because of the offset
    if time_offset > 0 :
        while data[i]['TIME'] < time_limit :
            i += 1


    # Analysing data points, ordered by TIME
    while i < len(data) :

        # Setting value for each pixel for that time window to zero
        for a in range(len(counted_events)) :
            for b in range(len(counted_events[a])) :
                counted_events[a][b].append(0)

        # Delimiting the new time window
        time_limit += time_interval

        # Computing the projection ratio for that frame of data
        projection_ratio = 1.0
        time_skipped = 0.0

        #
        # Going forward into the skipped periods list
        #

        # If the period to skip is wider than the interval time,
        # it is assumed that the amount of photons received is constant
        while j < len(gti) and gti[j][0] <= (time_limit - time_interval) and gti[j][1] >= time_limit :
            # Going to next interval
            time_limit += time_interval

            # Going forward into the interval list
            j += 1

            # Copying last value
            for a in range(len(counted_events)) :
                for b in range(len(counted_events[a])) :
                    counted_events[a][b][k] = counted_events[a][b][k - 1]

        # Case when the gti list is not over
        if j < len(gti) :

            # Case when the analysed time frame starts into a skipped time window
            if gti[j][0] <= (time_limit - time_interval) and gti[j][1] <= time_limit :
                time_skipped += time_limit - time_interval + gti[j][1]
                j += 1

            # If there is several cut time sections into the time window
            while j < len(gti) and gti[j][0] < time_limit and gti[j][1] <= time_limit :
                time_skipped += gti[j][1] - gti[j][0]
                j += 1

            # Case when the analysed time frame ends into a skipped time window
            if j < len(gti) and gti[j][0] < time_limit <= gti[j][1] :
                time_skipped += time_limit - gti[j][0]

            # Going forward into the events list
            while i < len(data) and data[i]['TIME'] <= time_limit:
                counted_events[int(data[i]['RAWX'])-1][int(data[i]['RAWY'])-1][len(counted_events[int(data[i]['RAWX'])-1][int(data[i]['RAWY'])-1])-1] += 1
                i += 1

            projection_ratio = time_skipped / time_interval

            # Ignoring the time window if there is too much time skipped
            if projection_ratio < acceptable_ratio :
                for a in range(len(counted_events)) :
                    for b in range(len(counted_events[a])) :
                        counted_events[a][b].pop(-1)

                k -= 1

            # Applying modificators on the counter according to the skipped time frames
            #elif time_skipped > 0.0 :
            elif time_skipped > 0.0 :
                for a in range(len(counted_events)) :
                    for b in range(len(counted_events[a])) :
                        counted_events[a][b][k] /= projection_ratio

                # Adding the time window
                time_windows.append(time_limit - time_interval)


            k += 1

    return counted_events, time_windows


###################################################################################################


def count_events(data, gti, time_interval, additional_tw, acceptable_ratio, start_time) :
    """
    Function calling the count_events procedure for each shifts. Usable only on for a CCD. See documentation.
    @param data:    E round, the list of events sorted by their TIME attribute
    @param gti:     G round, the list of TW cut-off the observation
    @param time_interval:   The duration of a time window
    @param additional_tw:   The number of additional TWs obtained by shifting the first TW
    @param acceptable_ratio:  The acceptability ratio for a TW
    @param start_time:  The t0 instant of the observation
    @return: The matrix M round; The list of the kept TWs
    """
    counted_events = []
    for i in range(64) :
        counted_events.append([])
        for j in range(200) :
            counted_events[i].append([])

    time_windows = []

    for i in range(1, additional_tw + 1) :
        counted_events_tmp, tws_tmp = __perform_count_events(data, gti, time_interval, (time_interval / additional_tw), acceptable_ratio, start_time)
        
        for i in range(64) :
            for j in range(200) :
                counted_events[i][j].extend(counted_events_tmp[i][j])
        time_windows.extend(tws_tmp)

    counters = [0 for _ in range(len(time_windows))]

    # Creating an index of time windows
    list_tws = [(time_windows[i], i) for i in range(len(time_windows))]

    list_tws = sorted(list_tws, key=lambda a: a[1])

    counted_events_output = []
    time_windows_output = []

    for i in range(64) :
        counted_events_output.append([])
        for j in range(200) :
            counted_events_output[i].append([])


    for tw in list_tws :
        for i in range(len(counted_events)) :
            for j in range(len(counted_events[i])) :
                counted_events_output[i][j].append(counted_events[i][j][tw[1]])
        time_windows_output.append(tw[0])

    return counted_events_output, time_windows_output




###################################################################################################
#                                                                                                 #
#                                                                                                 #
# Procedure variability_calculation                                                               #
#                                                                                                 #
#                                                                                                 #
###################################################################################################


def variability_calculation(counted_events) :
    """
    Implementation of variability_calculation procedure. Usable for a CCD. See documentation.
    @param counted_events:  The matrix M round (output of count_events)
    @return: The V_matrix, containing the variability of each pixel
    """

    V_matrix = []
    for i in range(len(counted_events)) :
        V_matrix.append([])
        
        for j in range(len(counted_events[i])) :
            V_matrix[i].append(0)

    med = 0.0
    # Computing V_matrix
    for i in range(len(V_matrix)) :        
        for j in range(len(V_matrix[i])) :
            median = numpy.median(counted_events[i][j])
            max_counted_events = max(counted_events[i][j])
            min_counted_events = min(counted_events[i][j])
            V_matrix[i][j] = max(max_counted_events - median, abs(min_counted_events - median))

    return V_matrix


###################################################################################################


def variability_computation_avg(gti, time_interval, acceptable_ratio, start_time, data) :
    """
    Function implementing the variability calculation using average technique.
    @param  data:    E round, the list of events sorted by their TIME attribute
    @param  gti:     G round, the list of TW cut-off the observation
    @param  time_interval:   The duration of a time window
    @param  acceptable_ratio:  The acceptability ratio for a TW
    @param  start_time:  The t0 instant of the observation
    @return: The matrix V_round
    """
    index_beg = 0
    index_end = 0

    # Building the output
    avg_counters = []
    counter_tmp = []
    counter_min = []
    counter_max = []
    V_mat = []

    for i in range(64) :
        avg_counters.append([])
        counter_tmp.append([])
        counter_min.append([])
        counter_max.append([])
        V_mat.append([])
        
        for j in range(200) :
            avg_counters[i].append(0.0)
            counter_tmp[i].append(0.0)
            counter_min[i].append(inf)
            counter_max[i].append(-inf)
            V_mat[i].append(0.0)
   


    i = 0 # Counter for data points
    j = 0 # Counter for cutted-off periods
    k = 0 # Counter of time frames

    projection_ratio = 1.0
    time_skipped = 0.0

    time_limit = start_time

    # Analysing data points, ordered by TIME
    while i < len(data) :

        # Setting value for each pixel for that time window to zero
        for a in range(len(avg_counters)) :
            for b in range(len(avg_counters[a])) :
                avg_counters[a][b] = 0
                counter_tmp[a][b] = 0

        # Delimiting the new time window
        time_limit += time_interval

        # Computing the projection ratio for that frame of data
        projection_ratio = 1.0
        time_skipped = 0.0

        #
        # Going forward into the skipped periods list
        #

        # If the period to skip is wider than the interval time,
        # it is assumed that the amount of photons received is constant
        while j < len(gti) and gti[j][0] <= (time_limit - time_interval) and gti[j][1] >= time_limit :
            # Going to next interval
            time_limit += time_interval

            # Going forward into the interval list
            j += 1

        # Case when the gti list is not over
        if j < len(gti) :

            # Case when the analysed time frame starts into a skipped time window
            if gti[j][0] <= (time_limit - time_interval) and gti[j][1] <= time_limit :
                time_skipped += time_limit - time_interval + gti[j][1]
                j += 1

            # If there is several cut time sections into the time window
            while j < len(gti) and gti[j][0] < time_limit and gti[j][1] <= time_limit :
                time_skipped += gti[j][1] - gti[j][0]
                j += 1

            # Case when the analysed time frame ends into a skipped time window
            if j < len(gti) and gti[j][0] < time_limit <= gti[j][1] :
                time_skipped += time_limit - gti[j][0]

            # Going forward into the events list
            while i < len(data) and data[i]['TIME'] <= time_limit:
                counter_tmp[int(data[i]['RAWX'])-1][int(data[i]['RAWY'])-1] += 1
                i += 1

            projection_ratio = time_skipped / time_interval

            # Computing the average for each pixel
            if projection_ratio > acceptable_ratio :
                for a in range(len(avg_counters)) :
                    for b in range(len(avg_counters[a])) :
                        counter_tmp[a][b] /= projection_ratio
                        avg_counters[a][b] = ((k * avg_counters[a][b]) + counter_tmp[a][b]) / (k + 1)
                        counter_min[a][b] = min(counter_min[a][b], counter_tmp[a][b])
                        counter_max[a][b] = max(counter_max[a][b], counter_tmp[a][b])
                k += 1


    # Computing variability
    for a in range(len(avg_counters)) :
        for b in range(len(avg_counters[a])) :
            V_mat[a][b] = max(counter_max[a][b] - avg_counters[a][b], abs(counter_min[a][b] - avg_counters[a][b]))

    return V_mat




###################################################################################################
#                                                                                                 #
#                                                                                                 #
# Procedure variable_areas_detection                                                              #
#                                                                                                 #
#                                                                                                 #
###################################################################################################


def __box_computations(variability_matrix, x, y, box_size) :
    """
    Function summing the variability values into a box.
    @param variability_matrix:  The V round matrix
    @param x:   The x coordinate of the top-left corner of the box
    @param y:   The y coordinate of the top-left corner of the box
    @param box_size:   The length of a side of a box
    @return: The sum of the variability for each pixel of the box
    """
    assert x <= 63 - box_size
    print(y)
    assert y <= 199 - box_size

    cpt = 0

    for i in range(x, x + box_size) :
        for j in range(y, y + box_size) :
            cpt += variability_matrix[x][y]

    return cpt


###################################################################################################


def __add_to_detected_areas(x, y, box_size, detected_areas) :
    """
    Function summing the variability values into a box.
    @param x:   The x coordinate of the top-left corner of the box
    @param y:   The y coordinate of the top-left corner of the box
    @param box_size:   The length of a side of a box
    @param detected_areas:  The A round set, containing already detected areas
    @return: The sum of the variability for each pixel of the box
    """
    box_set = {(a, b) for a in range(x, x + box_size) for b in range(y, y + box_size)}

    inserted = False
    i = 0
    while (not inserted) and i < len(detected_areas) :
        # If a part of the box has already been detected, the two sets of coordinates are merged...
        if len(box_set & detected_areas[i]) > 1 :
            detected_areas[i] |= box_set
            # ...and the loop is over
            inserted = True
        i += 1

    # If there has not been any merges :
    if not inserted :
        detected_areas.append(box_set)


###################################################################################################


def variable_areas_detection(lower_limit, box_size, detection_level, variability_matrix) :
    """
    Function detecting variable areas into a variability_matrix.
    @param lower_limit:         The lower_limit value is the smallest variability value needed to consider a pixel variable
    @param box_size:            The size of the box (optionnal, default = 3)
    @param detection_level:     A factor for the limit of detection
    @param variability_matrix:  The matrix returned by variability_calculation
    @return: A list of sets of coordinates for each area detected as variable
    """

    output = []

    box_count = 0

    for i in range(len(variability_matrix) - box_size) :
        j = 0
        while j < len(variability_matrix[i]) - box_size :
            box_count = __box_computations(variability_matrix, i, j, box_size)
            
            # If there's nothing into the box, it is completely skipped
            if box_count == 0 :
                j += box_size

            else :
                if box_count > detection_level * ((box_size**2) * lower_limit) :
                    __add_to_detected_areas(i, j, box_size, output)

                j += 1

    return output